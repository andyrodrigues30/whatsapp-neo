# Whatsapp Neo

A chrome extension that brings the trendy neomorphic design to Web Whatsapp.

## Getting Started

There will be an official public version on chrome webstore available soon.

To install, for now just:

1. Git clone the repository.
2. Go to `chrome://extensions`.
3. Enable developer mode _(toggle is top-right)_, if you haven't done so already.
4. Click on `Load Unpacked` and select the `src` directory.
